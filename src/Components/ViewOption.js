function ViewOption({ filter, sort }) {
	return (
		<div className="row m-1 p-3 px-5 justify-content-center">
			<div className="col-auto d-flex flex-end align-items-center">
				<label className="text-secondary my-2 pr-2 view-opt-label">
					Filter
				</label>
				<select
					className="custom-select custom-select-sm btn my-2 dropBox text-secondary"
					onChange={(e) => filter(e.target.value)}
				>
					<option value="all" selected>
						All
					</option>
					<option value="completed">Completed</option>
					<option value="active">Active</option>
				</select>
			</div>
			<div className="col-auto d-flex align-items-center px-1 pr-3">
				<label className="text-secondary my-2 pr-2 view-opt-label">Sort</label>
				<select
					className="custom-select custom-select-sm btn my-2 dropBox text-secondary"
					onChange={(e) => sort(e.target.value)}
				>
					<option value="status" selected>
						Status
					</option>
					<option value="alphabetically">Alphabetically</option>
				</select>
				<i
					className="fa fa fa-sort-amount-asc text-info btn mx-0 px-0 pl-1"
					data-toggle="tooltip"
					data-placement="bottom"
					title="Ascending"
				></i>
				<i
					className="fa fa fa-sort-amount-desc text-info btn mx-0 px-0 pl-1 d-none"
					data-toggle="tooltip"
					data-placement="bottom"
					title="Descending"
				></i>
			</div>
		</div>
	);
}

export default ViewOption;
