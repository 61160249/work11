import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import React from "react";
function ModalTodo({ show, onHide, todo, updateTodoText }) {
  const [input, setInput] = React.useState(todo.text);
  const handleSubmit = (e) => {
    e.preventDefault();
    updateTodoText(input);
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header className="d-flex align-items-center modal-body text-light" closeButton>
        <Modal.Title id="contained-modal-title-vcenter" className="text-light">Edit todo</Modal.Title>
      </Modal.Header>
      <Modal.Body className="d-flex justify-content-center m-body text-light">
        <form
          className="d-flex form-group flex-column align-items-center"
          onSubmit={handleSubmit}
        >
          <label htmlFor="todo" className="d-flex flex-row align-items-center">
            Task:
            <input
              name="todo"
              className="m-2 form-control text-light"
              type="text"
              value={input}
              onChange={(e) => setInput(e.target.value)}
              style={{backgroundColor: '#393e52'}}
            />
          </label>
          <Button type="submit" className="w-50">
            Change
          </Button>
        </form>
      </Modal.Body>
      {/* <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer> */}
    </Modal>
  );
}

export default ModalTodo;
